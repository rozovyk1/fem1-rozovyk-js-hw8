/*
Задание
Создать поле для ввода цены с валидацией.

Технические требования:

При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений.
Поведение поля должно быть следующим:
При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price.
span со значением при этом не создается.

В папке img лежат примеры реализации поля ввода и создающегося span.*/

//Creating span element
let currentPriceSpan = document.createElement('span');
currentPriceSpan.className = 'current-price-span';
currentPriceSpan.innerHTML = `Current price: $ `;
document.body.appendChild(currentPriceSpan);


// Creating price validation form
let priceValidationArea = document.createElement('form');
priceValidationArea.className = 'price-validation-area';
document.body.appendChild(priceValidationArea);

let priceLabel = document.createElement('label');
priceLabel.className = 'price-label';
priceValidationArea.appendChild(priceLabel);
priceLabel.innerHTML = 'Price, $';

//Adding input into the price validation area
let priceInput = document.createElement('input');
priceInput.className = 'price-input';
priceValidationArea.appendChild(priceInput);

let currentPriceValue = document.createElement('span');
currentPriceValue.className = 'current-price-value';
currentPriceSpan.appendChild(currentPriceValue);

//Creating remove button
let removeButton = document.createElement('button');
removeButton.className = 'remove-button';
removeButton.innerHTML = 'x';
currentPriceSpan.appendChild(removeButton);

//Adding/removing error message and checking if it exists
priceInput.addEventListener('blur', (event) => {
    let price = event.target.value;
    if (price < 0 || price == '') {
        priceInput.className = 'price-input price-input-error';
        if (!isErrorMessage()) {
            addErrorMessage();
            currentPriceSpan.style.display = 'none';
        }
    } else {
        if (isErrorMessage()) removeErrorMessage();
        currentPriceValue.innerHTML = event.target.value;
        currentPriceSpan.style.display = 'inline-block';
        priceInput.className = 'price-input price-input-blur';
    }
});

//Removing current price span
removeButton.addEventListener('click', (event) => {
    currentPriceSpan.style.display = 'none';
    priceInput.className = 'price-input';
    priceInput.value = "";
});

function addErrorMessage() {
    let errorMessage = document.createElement('span');
    priceValidationArea.appendChild(errorMessage);
    errorMessage.className = 'error';
    errorMessage.id = 'error-message';
    errorMessage.innerHTML = 'Please enter correct price';
}

function removeErrorMessage() {
    let errorMessage = document.getElementById('error-message');
    errorMessage.parentNode.removeChild(errorMessage);
}

function isErrorMessage() {
    return document.getElementById('error-message');
}